import { Injectable } from '@nestjs/common';
import { ProductDTO } from 'src/dto/product.dto';

@Injectable()
export class ProductService {
    private products: ProductDTO[] = [
        {name: 'Mango' , id: 1 ,price: 225},
        {name: 'Apple' , id: 2 ,price: 150},
        {name: 'Banaba' , id: 3 ,price: 100},
        {name: 'PineApple' , id: 4 ,price: 250},
    ];

    findAll(): ProductDTO[]{
        return this.products;
    } 
    findById(id:number){
        return this.products.find((p)=> p.id === id);
    }
    findBuCondition(predicate: (product: ProductDTO)=>boolean){
        return this.products.filter(p=>predicate(p));
    }
}
