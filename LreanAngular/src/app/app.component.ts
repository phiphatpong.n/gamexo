import { Component } from '@angular/core';
import { Product } from './shopping/model/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
customerList : string[] = ['customer1','customer2','customer3','customer4']

  valueprice = 0 ;
  value2: string = 'Hello PrimeNg' ;
  cars = [
    {
      year:1990,
      brand:'honda'
    },
    {
      year:1991,
      brand:'honda'
    },
    {
      year:1992,
      brand:'honda'
    },
    {
      year:1993,
      brand:'honda'
    },
    {
      year:1994,
      brand:'honda'
    },
    {
      year:1995,
      brand:'honda'
    },
    {
      year:1996,
      brand:'honda'
    }
  ];

  appMinLabel = 'myAppMinLabel';
  appMaxLabel =  'myAppMaxLabel';

  sqaureWidth = 250;
  sqaureHeigth = 100 ; 

  appcounter = 20 ;

  activate: boolean = false ;

  calBuffet(valuep: string){
    const price = Number(valuep);
    this.valueprice = (price*3)/4;
  }

  testClick(){
    console.log('test EventBinding')
  }

  testNumberChange(value1:number){
    console.log('test NumberChange from app action bar',value1);
  }

  doAppMaxChange(value2:number){
    console.log('test doAppMinChange event',value2);
  }
  doAppMinChange(value3:number){
    console.log('test maxChange event',value3);
  }

  title = 'LreanAngular';
  ninjaName = 'Naruto' ;

  ninjaConsole(){
    console.log("ninjaConsole",this.ninjaName);
  }

  ChangeNinjaName(name: string){
    this.ninjaName = name ;
  }

  pushCustomer(){
    this.customerList.push('customer'+(this.customerList.length+1))
  }
  unshiftCustomer(){
    this.customerList.push('customer'+(this.customerList.length+1))
  }
  spliceCustomer(index: number){
    this.customerList.splice(index,1);
  }

  products: Product[] = [
    {name: 'Banana' , description: 'ผลไม้' , price: 20},
    {name: 'Apple' , description: 'ผลไม้' , price: 30},
    {name: 'Cherry' , description: 'ผลไม้' , price: 40},
    {name: 'Melon' , description: 'ผลไม้' , price: 40},
    {name: 'Mango' , description: 'ผลไม้' , price: 50},
  ];

  filteredProduct: Product[] = this.products;

  searchProduct(text: string){
    this.filteredProduct = this.products.filter(product=>{
      const productName =  product.name.toLowerCase();
      const searchName = text.toLowerCase();
      return productName.indexOf(searchName)!==-1
    })
    console.log(text)
  }

}
