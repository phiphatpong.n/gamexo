import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionBarComponent } from './action-bar/action-bar.component';
import { MaxMinMeterComponent } from './max-min-meter/max-min-meter.component';
import { SqaureFlexComponent } from './sqaure-flex/sqaure-flex.component';
import {HttpClientModule} from '@angular/common/http';
import { TestRequestGetComponent } from './test-request/test-request-get/test-request-get.component'
import { TestRequestModule } from './test-request/test-request.module';
import { FormsModule } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {TableModule} from 'primeng/table';
import { ShoppingModule } from './shopping/shopping.module';
import { ShoppingSearchComponent } from './shopping/shopping-search/shopping-search.component';


@NgModule({
  declarations: [
    AppComponent,
    ActionBarComponent,
    MaxMinMeterComponent,
    SqaureFlexComponent,
    TestRequestGetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TestRequestModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ShoppingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    TestRequestGetComponent,
    ShoppingSearchComponent
  ]
})
export class AppModule { }
