import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { auditTime, debounceTime } from 'rxjs';

@Component({
  selector: 'app-shopping-search',
  templateUrl: './shopping-search.component.html',
  styleUrls: ['./shopping-search.component.scss']
})
export class ShoppingSearchComponent implements OnInit {

  valueSearch: string | undefined
  @Output() onInput = new EventEmitter<string>();
  @Output() onSearch = this.onInput.pipe(debounceTime(400)) ;
  constructor() { }

  ngOnInit(): void {
  }
  inputSearch(inputEl: string){
    this.onInput.emit(inputEl);
    // console.log(inputEl);
  }

}
