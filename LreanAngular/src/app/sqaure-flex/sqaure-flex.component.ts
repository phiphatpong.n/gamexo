import { Component, Input, OnInit } from '@angular/core';
import { UnsubscriptionError } from 'rxjs';

@Component({
  selector: 'app-sqaure-flex',
  templateUrl: './sqaure-flex.component.html',
  styleUrls: ['./sqaure-flex.component.scss']
})
export class SqaureFlexComponent implements OnInit {
  @Input() divwidth = 200 ;
  @Input() divheight = 200 ; 

  constructor() { }

  ngOnInit(): void {
  }

}
